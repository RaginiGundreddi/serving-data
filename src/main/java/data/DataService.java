package data;


public interface DataService {

    String[] getFilteredData(String searchCriteria);

}
