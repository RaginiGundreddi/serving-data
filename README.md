##servind-data

## Pre-installation Software Requirements
> 1. [gradle 2.3+] (https://gradle.org/)
>
> 2. [git v2.10.x+](https://git-scm.com/)
>
> 3. [JDK 1.8+]
>

## gradle configuration for local Development
> gradle installation creates an init.gradle file
> The following configuration is required in order to run the Backend via gradle:
>


```
allprojects {
    repositories {
        mavenLocal()
    }
}
```



## Project Installation
> **Project Installation occurs from terminal or command prompt**
>
> 1. Clone project from gitlab
>
> 2. Navigate to new project directory
>    `cd <path-to-cloned-project-directory>`
>
> 3. installation
>    `gradle build`
>
> 4. Build
>    `./gradlew bootRun`
>
> 5. Other way to build (cloning git will also include the jar in the project structure)
>    `java -jar fruits-rest-service-0.1.0.jar`
